/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parserhorarios;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author Jonathan
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader br = null;
        try {
            //prepare the building of the document
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            //Insert document root
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("grupos");
            doc.appendChild(rootElement);
            //read original file
            br = new BufferedReader(new FileReader("grupos.txt"));
            String line = br.readLine();
            //split it in lines and work them
            int x = 0;
            while ((line = br.readLine()) != null) {
                String[] values = line.split("~", -1);
                Element prevNode = doc.getElementById(values[0] + "-" + values[1]);
                if(prevNode != null){
                    System.out.println(values[0] + "-" + values[1]);
                    Node lunes = prevNode.getElementsByTagName("lunes").item(0);
                    Element horario_l = doc.createElement("horario");
                    horario_l.setAttribute("hora", values[10]);
                    horario_l.setAttribute("salon", values[11]);
                    lunes.appendChild(horario_l);
                    Node martes = prevNode.getElementsByTagName("martes").item(0);
                    Element horario_m = doc.createElement("horario");
                    horario_m.setAttribute("hora", values[12]);
                    horario_m.setAttribute("salon", values[13]);
                    martes.appendChild(horario_m);
                    Node miercoles = prevNode.getElementsByTagName("miercoles").item(0);
                    Element horario_mi = doc.createElement("horario");
                    horario_mi.setAttribute("hora", values[14]);
                    horario_mi.setAttribute("salon", values[15]);
                    miercoles.appendChild(horario_mi);
                    Node jueves = prevNode.getElementsByTagName("jueves").item(0);
                    Element horario_j = doc.createElement("horario");
                    horario_j.setAttribute("hora", values[16]);
                    horario_j.setAttribute("salon", values[17]);
                    jueves.appendChild(horario_j);
                    Node viernes = prevNode.getElementsByTagName("viernes").item(0);
                    Element horario_v = doc.createElement("horario");
                    horario_v.setAttribute("hora", values[18]);
                    horario_v.setAttribute("salon", values[19]);
                    viernes.appendChild(horario_v);
                    Node sabado = prevNode.getElementsByTagName("sabado").item(0);
                    Element horario_s = doc.createElement("horario");
                    horario_s.setAttribute("hora", values[20]);
                    horario_s.setAttribute("salon", values[21]);
                    sabado.appendChild(horario_s);
                    Node domingo = prevNode.getElementsByTagName("domingo").item(0);
                    Element horario_d = doc.createElement("horario");
                    horario_d.setAttribute("hora", values[22]);
                    horario_d.setAttribute("salon", values[23]);
                    domingo.appendChild(horario_d);
                }else{
                    // Group element
                    Element grupo = doc.createElement("grupo");
                    rootElement.appendChild(grupo);
                    // set attributes to element
                    grupo.setAttribute("ID", values[0] + "-" + values[1]);
                    grupo.setIdAttribute("ID", true);
                    grupo.setAttribute("clave_grupo", values[0]);
                    grupo.setAttribute("clave_materia", values[1]);
                    grupo.setAttribute("materia", values[2]);
                    grupo.setAttribute("modalidad", values[3]);
                    grupo.setAttribute("profesor", values[5]);
                    grupo.setAttribute("cupo", values[6]);
                    grupo.setAttribute("inscritos", values[7]);
                    grupo.setAttribute("tipo_grupo", values[8]);
                    //working with the scheduled days
                    //lunes
                    Element lunes = doc.createElement("lunes");
                    Element horario_l = doc.createElement("horario");
                    horario_l.setAttribute("hora", values[10]);
                    horario_l.setAttribute("salon", values[11]);
                    lunes.appendChild(horario_l);
                    grupo.appendChild(lunes);
                    //martes
                    Element martes = doc.createElement("martes");
                    Element horario_ma = doc.createElement("horario");
                    horario_ma.setAttribute("hora", values[12]);
                    horario_ma.setAttribute("salon", values[13]);
                    martes.appendChild(horario_ma);
                    grupo.appendChild(martes);
                    //miercoles
                    Element miercoles = doc.createElement("miercoles");
                    Element horario_mi = doc.createElement("horario");
                    horario_mi.setAttribute("hora", values[14]);
                    horario_mi.setAttribute("salon", values[15]);
                    miercoles.appendChild(horario_mi);
                    grupo.appendChild(miercoles);
                    //jueves
                    Element jueves = doc.createElement("jueves");
                    Element horario_j = doc.createElement("horario");
                    horario_j.setAttribute("hora", values[16]);
                    horario_j.setAttribute("salon", values[17]);
                    jueves.appendChild(horario_j);
                    grupo.appendChild(jueves);
                    //viernes
                    Element viernes = doc.createElement("viernes");
                    Element horario_v = doc.createElement("horario");
                    horario_v.setAttribute("hora", values[18]);
                    horario_v.setAttribute("salon", values[19]);
                    viernes.appendChild(horario_v);
                    grupo.appendChild(viernes);            
                    //sabado
                    Element sabado = doc.createElement("sabado");
                    Element horario_s = doc.createElement("horario");
                    horario_s.setAttribute("hora", values[20]);
                    horario_s.setAttribute("salon", values[21]);
                    sabado.appendChild(horario_s);
                    grupo.appendChild(sabado);
                    //domingo
                    Element domingo = doc.createElement("domingo");
                    Element horario_d = doc.createElement("horario");
                    horario_d.setAttribute("hora", values[22]);
                    horario_d.setAttribute("salon", values[23]);
                    domingo.appendChild(horario_d);
                    grupo.appendChild(domingo);
                }
            }
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            //Real file output
            StreamResult result = new StreamResult(new File("grupos.xml"));

            // Output to console for testing
            //StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");
        }
        catch (TransformerException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }        finally {
          try {
            if (br != null)
              br.close();
          }
          catch (IOException ex) {
            ex.printStackTrace();
          }
        }
    }
    
}
