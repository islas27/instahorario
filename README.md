# instaHorario #

Software que genera horarios para los estudiantes de acuerdo a los criterios seleccionados como materias elegidas, horas aceptables para tomar clases y preferencia de más materias o menos horas libres entre clases.

Algoritmo original desarrollado en una tesis para la universidad de Chihuahua por otros desarrolladores.

### Versión ###

* Versión 2.2 (Actualización del título 2016B y bug solucionado al quitar materias de la lista)

#### Versiones anteriores ####

* Versión 2.1 (Base de datos actualizada para el semestre 2016B)
* Versión 2.0 (Base de datos actualizada para el semestre 2016A)

### ¿Cómo funciona? ###

Solo bajar el archivo comprimido, y por medio de Firefox (Safari en MacOS/OS X también funciona correctamente) abrir el index.html (todo es dentro del navegador).
