/**
 * Created by Jonathan on 29/06/2015.
 */

var slider_selector = null;

function loadXMLDoc(filename){
    xhttp = new XMLHttpRequest();
    xhttp.open("GET",filename,false);
    xhttp.send();
    return xhttp.responseXML;
}

function generar_tabla_horarios(){
    document.getElementById("contenido-seleccion").setAttribute("hidden","true");
    document.getElementById("contenido-horario").removeAttribute("hidden");
    slider_selector = new Slider("#slider_selector", {
        min: 1,
        max: resultXML.querySelectorAll("tabla").length,
        value: 1
    });
    slider_selector.on("change", function(slideEvt) {
        cambiar_tablas(slideEvt.newValue);
    });
    cambiar_tablas(1);
}

function cambiar_selector(cantidad){
    var newValue = slider_selector.getValue() + cantidad;
    slider_selector.setValue(newValue, true, true);
}

function cambiar_tablas(id){
    var busqueda = 'tabla[ID="'+ id +'"]';
    var tablaSel = resultXML.querySelector(busqueda);
    var materiasSel = tablaSel.querySelectorAll("materia");
    //Creamos una reticula vacia para guardar ahi el horario
    var horario = [];
    for(var i=0;i<15;i++){
        horario[i] = [];
        for (var j = 0; j < 7;j++ ) {
            horario[i][j] = "";
        }
    }
    //Segunda Reticula para el listado de materias-profesores
    var lista = [];
    for(i = 0; i< materiasSel.length; i++){
        console.log(materiasSel[i]);
        var ID = materiasSel[i].getAttribute("grupo") + "-" + materiasSel[i].getAttribute("clave");
        busqueda = 'grupo[ID="'+ ID.trim() +'"]';
        console.log(busqueda);
        var nodoMateria = xmlDoc.querySelector(busqueda);
        console.log(nodoMateria);
        var materia = new MateriaDatos(nodoMateria);
        lista.push(materia);
        horario = marcar_horario_tabla(horario, nodoMateria);
    }
    var tablaHorario = document.getElementById("tabla-horarios");
    var tablaMaestros = document.getElementById("tabla-maestros");
    //Creamos las tablas html
    var stringTable = "";
    for(i = 0; i < lista.length; i++){
        stringTable += '<tr>';
        stringTable += '<td>' + lista[i].clave +'</td>';
        stringTable += '<td>' + lista[i].grupo +'</td>';
        stringTable += '<td>' + lista[i].nombre +'</td>';
        stringTable += '<td>' + lista[i].profesor +'</td>';
        stringTable += '</tr>';
    }
    tablaMaestros.innerHTML = stringTable;
    stringTable = "";
    for(i = 0; i < 15; i++){
        stringTable += '<tr>';
        stringTable += '<td>' + columna_horas(i) +'</td>';
        for (j = 0; j < 7; j++) {
            stringTable += '<td>' + horario[i][j] +'</td>';
        }
        stringTable += '</tr>';
    }
    tablaHorario.innerHTML = stringTable;
}

var columna_horas = function(hora){
    return (hora+7) + ":00 - " + (hora+8) + ":00";
};

var marcar_horario_tabla = function(horario, nodoMateria){
    var lunes = obtener_horas_v2(nodoMateria.querySelectorAll("lunes horario"));
    horario = marcar_horario_v2(lunes, 0, horario, nodoMateria);
    var martes = obtener_horas_v2(nodoMateria.querySelectorAll("martes horario"));
    horario = marcar_horario_v2(martes, 1, horario, nodoMateria);
    var miercoles = obtener_horas_v2(nodoMateria.querySelectorAll("miercoles horario"));
    horario = marcar_horario_v2(miercoles, 2, horario, nodoMateria);
    var jueves = obtener_horas_v2(nodoMateria.querySelectorAll("jueves horario"));
    horario = marcar_horario_v2(jueves, 3, horario, nodoMateria);
    var viernes = obtener_horas_v2(nodoMateria.querySelectorAll("viernes horario"));
    horario = marcar_horario_v2(viernes, 4, horario, nodoMateria);
    var sabado = obtener_horas_v2(nodoMateria.querySelectorAll("sabado horario"));
    horario = marcar_horario_v2(sabado, 5, horario, nodoMateria);
    var domingo = obtener_horas_v2(nodoMateria.querySelectorAll("domingo horario"));
    horario = marcar_horario_v2(domingo, 6, horario, nodoMateria);
    return horario;
};

var marcar_horario_v2 = function(horas, dia, h, nodoMateria){
    if(horas){
        for(var q = horas[0]-7; q < horas[1]-7; q++){
            //Agregar info a la celda del horario
            h[q][dia] = nodoMateria.getAttribute("materia") + " - " + horas[2];
        }
        return h;
    }else{
        return h;
    }
};

var obtener_horas_v2 = function(elemento){
    //Esta funcion hace el parsing de los nodos horario, sin importar si estan vacios,
    //son de dos salones distintos, o es el mejor de los casos (un solo nodo horario x dia)
    var ho = [];
    if(elemento[0].getAttribute("hora") !== ""){
        if(elemento.length > 1){
            if(elemento[1].getAttribute("hora") !== ""){
                //Dos salones distintos
                var array = [[],[]];
                array[0][0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
                array[0][1] = elemento[0].getAttribute("hora"); array[0][2] = elemento[0].getAttribute("salon");
                array[1][0] = elemento[1].getAttribute("hora").split("-")[0].split(":")[0];
                array[1][1] = elemento[1].getAttribute("hora"); array[1][2] = elemento[1].getAttribute("salon");
                array.sort(function(a, b){return a[0] - b[0]});
                ho[0] = array[0][1].split("-")[0].split(":")[0];
                ho[1] = array[1][1].split("-")[1].split(":")[0];
                ho[2] = array[0][2]+ " -> " + array[1][2];
            }else{
                //Un solo salon
                ho[0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
                ho[1] = elemento[0].getAttribute("hora").split("-")[1].split(":")[0];
                ho[2] = elemento[0].getAttribute("salon");
            }
        }else{
            //Un solo nodo horario x dia
            ho[0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
            ho[1] = elemento[0].getAttribute("hora").split("-")[1].split(":")[0];
            ho[2] = elemento[0].getAttribute("salon");
        }
    }else{
        ho = null;
    }
    return ho;
};

function MateriaDatos(nodo){
    if(nodo != null){
        this.clave = nodo.getAttribute("clave_materia");
        this.nombre = nodo.getAttribute("materia");
        this.grupo = nodo.getAttribute("clave_grupo");
        this.profesor = nodo.getAttribute("profesor");
        this.ID = nodo.getAttribute("clave_grupo") + "-" + nodo.getAttribute("clave_materia");
    }
}
