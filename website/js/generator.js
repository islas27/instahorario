/**
 * Created by Jonathan on 04/07/2015.
 */

var obtener_horas = function(elemento){
    //Esta funcion hace el parsing de los nodos horario, sin importar si estan vacios,
    //son de dos salones distintos, o es el mejor de los casos (un solo nodo horario x dia)
    var ho = [];
    if(elemento[0].getAttribute("hora") !== ""){
        if(elemento.length > 1){
            if(elemento[1].getAttribute("hora") !== ""){
                var array = [[],[]];
                array[0][0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
                array[0][1] = elemento[0].getAttribute("hora");
                array[1][0] = elemento[1].getAttribute("hora").split("-")[0].split(":")[0];
                array[1][1] = elemento[1].getAttribute("hora");
                array.sort(function(a, b){return a[0] - b[0]});
                ho[0] = array[0][1].split("-")[0].split(":")[0];
                ho[1] = array[1][1].split("-")[1].split(":")[0];
            }else{
                ho[0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
                ho[1] = elemento[0].getAttribute("hora").split("-")[1].split(":")[0];
            }
        }else{
            ho[0] = elemento[0].getAttribute("hora").split("-")[0].split(":")[0];
            ho[1] = elemento[0].getAttribute("hora").split("-")[1].split(":")[0];
        }
    }else{
        ho = null;
    }
    return ho;
};

function natSort(as, bs){
    var a, b, a1, b1, i= 0, L, rx=  /(\d+)|(\D+)/g, rd=  /\d/;
    if(isFinite(as) && isFinite(bs)) return as - bs;
    a= String(as).toLowerCase();
    b= String(bs).toLowerCase();
    if(a=== b) return 0;
    if(!(rd.test(a) && rd.test(b))) return a> b? 1: -1;
    a= a.match(rx);
    b= b.match(rx);
    L= a.length> b.length? b.length: a.length;
    while(i < L){
        a1= a[i];
        b1= b[i++];
        if(a1!== b1){
            if(isFinite(a1) && isFinite(b1)){
                if(a1.charAt(0)=== "0") a1= "." + a1;
                if(b1.charAt(0)=== "0") b1= "." + b1;
                return a1 - b1;
            }
            else return a1> b1? 1: -1;
        }
    }
    return a.length - b.length;
}


var marcar_horario = function(horas, dia, h){
    if(horas){
        for(var q = horas[0]-7; q < horas[1]-7; q++){
            h[q][dia] = true;
        }
        return h;
    }else{
        return h;
    }
};
var sumatoria_horas = function(horas){
    return (horas)? horas[1]-horas[0]: 0;
};

//Clase Materia
function Materia(){
    //VARIABLES
        //String
    this.clave = null;
    this.nombre = null;
    this.grupo = null;
    this.homo = null;
        //booleans
    this.horario = [];
        //Int
    this.horas = 0;

    for(var i=0;i<15;i++){
        this.horario[i] = [];
        for (var j = 0; j < 7;j++ ) {
            this.horario[i][j] = false;
        }
    }
}

//Clase Generador
function Generador(listaMaterias, v, h_min, h_max){
    //VARIABLES
        //String
    var busqueda = "";
    var clave_materia = "";
    var clave_grupo = "";
        //Int
    this.intentos = 0;
    this.limite = 0;
    this.h = v;
    this.h_min = h_min-7;
    this.h_max = h_max-8;
    this.k = 20 - v;
        //Arrays
    this.homos = [];
    this.materias = [];
    this.nodos = [];

        //Construccion
    this.raiz = new Nodo(this);
    //cargar xml con lista de homologaciones
    var xmlDocHomo = loadXMLDoc("homologadas.xml");
    for(var i = 0; i < listaMaterias.length; i++){
        //Cargar materias posibles con las claves seleccionadas
         clave_materia = listaMaterias[i];
        busqueda = 'grupo[modalidad="PRESENCIAL"][clave_materia="'+ clave_materia +'"]';
        var materiasPosibles = xmlDoc.querySelectorAll(busqueda);
        for(var j = 0; j < materiasPosibles.length; j++){
            var m = new Materia();
            m.clave = materiasPosibles[j].getAttribute("clave_materia");
            m.homo = m.clave;
            m.nombre = materiasPosibles[j].getAttribute("materia");
            m.grupo = materiasPosibles[j].getAttribute("clave_grupo");
            this.materias.push(m);
        }
        busqueda = 'homo[clave="'+ clave_materia +'"]';
        var homologacion = xmlDocHomo.querySelectorAll(busqueda);
        //Cargar lista de homologadas de acuerdo a las claves seleccionadas
        for(j = 0; j < homologacion.length; j++){
            var a = [];
            a[0] = homologacion[j].getAttribute("clave");
            a[1] = homologacion[j].getAttribute("homologada");
            this.homos.push(a);
        }
        //agregar a la lista las materias homologas
        for(j = 0; j < this.homos.length; j++){
            clave_materia = this.homos[j][0];
            var clave_homo = this.homos[j][1];
            busqueda = 'grupo[modalidad="PRESENCIAL"][clave_materia="'+ clave_homo +'"]';
            materiasPosibles = xmlDoc.querySelectorAll(busqueda);
            for(var k = 0; k < materiasPosibles.length; k++){
                m = new Materia();
                m.clave = clave_materia;
                m.homo = clave_homo;
                m.nombre = materiasPosibles[k].getAttribute("materia");
                m.grupo = materiasPosibles[k].getAttribute("clave_grupo");
                this.materias.push(m);
            }
        }
    }
    //Parsing de horas a ocupar y llenado de los valores de los objetos materia
    for(i = 0; i < this.materias.length; i++){
        clave_materia = this.materias[i].homo;
        clave_grupo = this.materias[i].grupo;
        var ID = clave_grupo + "-" + clave_materia;
        busqueda = 'grupo[ID="'+ ID +'"]';
        var nodoResultado = xmlDoc.querySelector(busqueda);
        //Se separa dia a dia el obtener las horas ocupadas debido a que FF, Chr y Opera
        //cada whitespace lo toman como un nodo extra, asi puedo unicamente seleccionar los nodos
        //relevantes
        var lunes = obtener_horas(nodoResultado.querySelectorAll("lunes horario"));
        this.materias[i].horario = marcar_horario(lunes, 0, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(lunes);
        var martes = obtener_horas(nodoResultado.querySelectorAll("martes horario"));
        this.materias[i].horario = marcar_horario(martes, 1, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(martes);
        var miercoles = obtener_horas(nodoResultado.querySelectorAll("miercoles horario"));
        this.materias[i].horario = marcar_horario(miercoles, 2, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(miercoles);
        var jueves = obtener_horas(nodoResultado.querySelectorAll("jueves horario"));
        this.materias[i].horario = marcar_horario(jueves, 3, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(jueves);
        var viernes = obtener_horas(nodoResultado.querySelectorAll("viernes horario"));
        this.materias[i].horario = marcar_horario(viernes, 4, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(viernes);
        var sabado = obtener_horas(nodoResultado.querySelectorAll("sabado horario"));
        this.materias[i].horario = marcar_horario(sabado, 5, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(sabado);
        var domingo = obtener_horas(nodoResultado.querySelectorAll("domingo horario"));
        this.materias[i].horario = marcar_horario(domingo, 6, this.materias[i].horario);
        this.materias[i].horas += sumatoria_horas(domingo);
    }
    //Ordenar materias por numero de horas, mayor a menor
    this.materias.sort(function(a, b){return b.horas- a.horas});
    this.limite = this.materias.length * 25;
    this.raiz.expandir();
    //Generar tabla de resultados
}

//Clase Nodo
function Nodo(Generador){
    //VARIABLES
        //booleans
    this.check = []; //matriz de horario 7 dias * 15 horas posibles por las horas virtuales nocturnas
    for (var i = 0; i < 15; i++) {
        this.check[i] = [];
        for (var j = 0; j < 7; j++) {
            this.check[i][j] = false;
        }
    }
    this.visitado = false;
    this.expandido = false;
        //Arrays
    this.materias = [];
    this.hijos = [];
        //Strings
    this.nombreNodo;
        //Int
    this.fitness;
        //Generadores
    var gen = Generador;

    //METODOS

    this.esNodoHoja = function(){
        return (this.hijos.length === 0);
    };

    this.setNombre = function(materias){
        var nombreNuevo = "";
        for(var i = 0; i <  materias.length; i++){
            nombreNuevo = materias[i].homo + "-" + materias[i].grupo + " " + nombreNuevo;
        }
        try{
            var algo = nombreNuevo.trim().split(" ");
            algo.sort(function(a,b){return a.localeCompare(b)});
            nombreNuevo = "";
            for(var asdf = 0; asdf < algo.length; asdf++){
                nombreNuevo += " " + algo[asdf];
            }
            console.log(nombreNuevo)
        }catch(ex){
            nombreNuevo = "";
        }
        return nombreNuevo;
    };

    this.contarLibres = function(){
        var libres = 0;
        var li;
        var ls;
        var dia;
        for(dia = 0; dia < 7; dia++){
            for(li = 0; li < 15 && this.check[li][dia] == false; li++);
            for(ls = 13; ls >= 0 && this.check[ls][dia] == false; ls--);
            while(li < ls){
                if(this.check[li][dia] == false){
                    libres++;
                }
                li++;
            }
        }
        return libres;
    };

    this.getFitness = function(){
        this.fitness = -this.materias.length * gen.h + this.contarLibres() * gen.k;
    };

    this.recorrer = function(){
        if(!this.visitado){
            this.visitado = true;
            if(!this.esNodoHoja()){
                for(var i = 0; i < this.hijos.length; i++){
                    this.hijos[i].recorrer();
                }
            }else{
                //@TODO
                gen.acomodar(this);
            }
        }
    };

    this.repetido = function(nombre){
        for(var i = 0; i < gen.nodos.length; i++){
            if(nombre == gen.nodos[i].nombre){
                return gen.nodos[i];
            }
        }
        return null;
    };

    this.addMateria  = function(m){
        this.materias.push(m);
        for(var i = 0; i < 15; i++){
            for(var j = 0; j < 7; j++){
                if(m.horario[i][j]){
                    this.check[i][j] = true;
                }
            }
        }
    };

    this.empalme = function(m){
        for(var i = 0; i < 15; i++){
            for(var j = 0; j < 7; j++){
                if(this.check[i][j] && m.horario[i][j]){
                    return true;
                }
            }
        }
        return false;
    };

    this.dentroHorario = function(m){
        for(var q = 0; q < gen.h_min; q++){
            for(var w = 0; w < 7; w++){
                if(m.horario[q][w])return false;
            }
        }
        for(q = gen.h_max+1; q < 15; q++){
            for(w = 0; w < 7; w++){
                if(m.horario[q][w])return false;
            }
        }
        return true;
    };

    this.expandir = function(){
        if(gen.intentos < gen.limite && !this.expandido){
            this.expandido = true;
            for(var r = 0; r < gen.materias.length; r++){
                var esta = false;
                for(var o = 0; o < this.materias.length; o++){
                    if(this.materias[o].clave == gen.materias[r].clave){
                        esta = true;
                        break;
                    }
                }
                if(!esta && !this.empalme(gen.materias[r]) && this.dentroHorario(gen.materias[r])){
                    var hijo = new Nodo(gen);
                    for(o = 0; o < this.materias.length; o++){
                        hijo.addMateria(this.materias[o])
                    }
                    hijo.addMateria(gen.materias[r]);
                    hijo.nombre = hijo.setNombre(hijo.materias);
                    var nodo = this.repetido(hijo.nombre);
                    if(nodo != null){
                        this.hijos.push(nodo);
                    } else{
                        hijo.getFitness();
                        this.hijos.push(hijo);
                        gen.nodos.push(hijo);
                        gen.nodos.sort(function(a, b){return a.fitness - b.fitness})
                    }
                }
            }
            if(this.hijos.length > 0){
                this.hijos.sort(function(a, b){return a.fitness - b.fitness});
                for(o = 0; o < this.hijos.length; o++){
                    this.hijos[o].expandir();
                }
            }else {
                gen.intentos++;
            }
            return true;
        }else{
            return false;
        }
    };

}
